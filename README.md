LOC Calculator is a simple Java based tool for calculating lines of code of source files. Features include:

 * Scans a single file or a directory. Option to recursively scan entire directory.
 * Option to ignore white spaces.
 * Ignores binary files.

## Download

The tool can be downloaded from the [downloads area](https://bitbucket.org/satish_c/loc-calculator/downloads). Download the binary package.

## Prerequisites
A JRE version of 1.5 or higher is needed to run this tool

## Running the tool
Open the terminal and run the binary:
```
java -jar loc-calculator-1.1.jar
```

**OR**

If jar files are associated to open with Java, then you can double click the loc-calculator jar to run it.

## Screenshots
![Screenshot 1](https://bitbucket.org/repo/ejp6bz/images/1794122275-loc-1.png)
![Screenshot 2](https://bitbucket.org/repo/ejp6bz/images/3640311181-loc-2.png)
![Screenshot 3](https://bitbucket.org/repo/ejp6bz/images/2766256401-loc-3.png)