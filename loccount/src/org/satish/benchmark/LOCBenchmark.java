/*
 * Copyright (C) 2008 Satish Chandra
 *
 * This file is part of LOC Calculator.
 *
 * LOC Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOC Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOC Calculator.  If not, see <http://www.gnu.org/licenses/>. 
 */

package org.satish.benchmark;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.satish.core.LOCCount;

/**
 * @author satish
 * Created on Sep 15, 2007
 */
public class LOCBenchmark {
	
	private static final String path = "/home/satish/Programming/workspace-jsf/loccount/src";
	private LOCCount loccnt;

	/**
	 * 
	 */
	public LOCBenchmark() {
		super();
		loccnt = new LOCCount(path, true, false);
	}

	public void benchmarkJavaIO() throws IOException {

		int count = 10;
		List<Double> times = new ArrayList<Double>(count);
		Timer timer = new Timer();

		for (int i = 0; i < count; i++) {
			timer.reset();
			timer.start();
			loccnt.process();
			timer.end();
			times.add(timer.getTimeInMillis());
		}
		
		printResults(times, "benchmarkJavaIO");
	}

	private void printResults(List<Double> times, String name) {
		Collections.sort(times);
		long sum = 0;
		for (double time : times) {
			sum += time;
		}
		double avg = sum / (double) (times.size());

		System.out.println("============ " + name + " ============");
		System.out.println("Total Time : " + sum / 1000D + " secs");
		System.out.println("Max Time   : " + times.get(times.size() - 1) / 1000D + " secs");
		System.out.println("Avg Time   : " + avg / 1000D + " secs");
		System.out.println("Min Time   : " + times.get(0) / 1000D + " secs");
	}

	public static void main(String[] args) {
		LOCBenchmark benchmark = new LOCBenchmark();
		try {
			benchmark.benchmarkJavaIO();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
