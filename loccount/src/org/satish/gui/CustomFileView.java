/*
 * Copyright (C) 2008 Satish Chandra
 *
 * This file is part of LOC Calculator.
 *
 * LOC Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOC Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOC Calculator.  If not, see <http://www.gnu.org/licenses/>. 
 */
package org.satish.gui;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.filechooser.FileView;
import org.satish.core.Constants;

/**
 * Provides custom icons for files and directories.
 * 
 * @author satish
 */
public class CustomFileView extends FileView {
		
	//Pattern for extracting file extension
	private static Pattern pattern = Pattern.compile("\\.([^\\.]*)$");
	
	private static Map<String, List<String>> iconTypeMap = new HashMap<String, List<String>>();
	
	//Create a map of icon types and file extensions
	static {
		iconTypeMap.put(Constants.AUDIO,
				Arrays.asList(new String[]{"wav", "mp3", "flac", "wma", "ra"}));
		iconTypeMap.put(Constants.COMPRESSED,
				Arrays.asList(new String[]{"deb", "gz", "pkg", "rar", "zip", "jar", "war", "ear"}));
		iconTypeMap.put(Constants.IMAGE,
				Arrays.asList(new String[]{"jpeg", "jpg", "gif", "png", "tiff", "bmp"}));
		iconTypeMap.put(Constants.OFF_DOC,
				Arrays.asList(new String[]{"odt", "odf", "doc", "docx"}));
		iconTypeMap.put(Constants.PDF,
				Arrays.asList(new String[]{"pdf"}));
		iconTypeMap.put(Constants.PRESENTATION,
				Arrays.asList(new String[]{"odf", "ppt", "pptx"}));
		iconTypeMap.put(Constants.SOURCE_CODE,
				Arrays.asList(new String[]{"java", "c", "cpp", "js", "css", "sh", "py", "txt"}));
		iconTypeMap.put(Constants.SPREADSHEET,
				Arrays.asList(new String[]{"ods", "xls", "xlsx"}));
		iconTypeMap.put(Constants.VIDEO,
				Arrays.asList(new String[]{"avi", "mpg", "mpeg", "ogg", "mp4", "mkv", "wmv", "rm", "asf", "mov", "3gp", "flv", "m4v"}));
		iconTypeMap.put(Constants.WEB_PAGE,
				Arrays.asList(new String[]{"htm", "html", "xhtml", "jsp", "jspf", "jsf", "asp", "aspx", "php"}));
	}

	@Override
	public Icon getIcon(File file) {
		String iconType = getFileType(file);
		Icon icon = IconCache.getIcon(iconType);		
		return icon;
	}

	private static String getFileType(File file) {

		if (file.isDirectory()) {
			return Constants.DIRECTORY;
		}
		
		Matcher matcher = pattern.matcher(file.getName());
		
		//If file has an extension
		if (matcher.find()) {
			
			String extension = matcher.group(1);
			
			//Find iconType for the extention in the iconTypeMap
			for (String fileType : iconTypeMap.keySet()) {
				List list = iconTypeMap.get(fileType);
				if (list.contains(extension.toLowerCase())) {
					return fileType;
				}
			}
		}

		return Constants.GENERIC;
	}
}